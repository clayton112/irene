# This file is used to interface the numpy arrays generated elsewhere with the 
# Matplotlib graphics system, for use n generating meaningful data and understanding.
# No computation is done here.

import numpy as np
import matplotlib.pyplot as plt

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj



# Plot a list of several curves. 
# 
# ARGS:
# 	List xlists: List of Arrays of X values of curves to be plotted.
# 	List ylists: Corresponding List of Arrays of Y values of curves to be plotted.
# 	String plttype: Type of plot to generate. Only changes labels of axes.
# 	Boolean dots: If True, plot a second time, showing individual points with connecting curves.
#	int maxlength: Only plot up to a finite number of points per curve. 
# RETURNS:
#	None
def plot_curve(xlists,ylists, plttype = 'time', dots = False, maxlength=50000,labels = None):
	if labels:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.plot(xlists[i][:maxlength],ylists[i][:maxlength],label = labels[i])
			else: 
				plt.plot(xlists[i],ylists[i]) 
	else:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.plot(xlists[i][:maxlength],ylists[i][:maxlength])
			else: 
				plt.plot(xlists[i],ylists[i]) 
	print(str(i+1)+" Curves Plotted")
	# Show constant values used during generation of this data.
	const.printout()
	if plttype=='time':
		plt.xlabel("x position (cm)")
		plt.ylabel("y position (cm)")
		plt.title("Sample Needle Position for R = "+str(const.R)+" cm")
	elif plttype=='filter':
		plt.xlabel("Frequency f")
		plt.ylabel("Normalized RMS Derivative Amplitude")
		plt.title("Sample Filter for R = "+str(const.R) +" cm")
	elif plttype=='fft':
		plt.xlabel("Frequency f")
		plt.ylabel("Amplitude")
		plt.title("FFT of data")
	plt.legend()
	if dots:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.plot(xlists[i][:maxlength],ylists[i][:maxlength],"o")
			else: 
				plt.plot(xlists[i],ylists[i],"o")
	plt.show()

# Plot a list of several curves, scaled logarithmically on both axes. Only used to plot FFTs. 
# 
# ARGS:
# 	List xlists: List of Arrays of X values of curves to be plotted.
# 	List ylists: Corresponding List of Arrays of Y values of curves to be plotted.
# 	Boolean dots: If True, plot a second time, showing individual points with connecting curves.
#	int maxlength: Only plot up to a finite number of points per curve. 
# RETURNS:
#	None
def plot_log(xlists,ylists,dots = False,maxlength = 50000,labels = None):
	if labels:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.loglog(xlists[i][:maxlength],ylists[i][:maxlength],label = labels[i])
			else: 
				plt.loglog(xlists[i],ylists[i],label = labels[i]) 
	else:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.loglog(xlists[i][:maxlength],ylists[i][:maxlength])
			else: 
				plt.loglog(xlists[i],ylists[i]) 
	print(str(i+1)+" Curves Plotted")
	# Show constant values used during generation of this data.
	const.printout()
	plt.xlabel("Frequency f")
	plt.ylabel("Amplitude")
	plt.title("FFT of R = "+str(const.R)+" cm filtered data")
	plt.legend()
	if dots:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.plot(xlists[i][:maxlength],ylists[i][:maxlength],"o")
			else: 
				plt.plot(xlists[i],ylists[i],"o")
	plt.show()

# Plot a list of several curves, scaled logarithmically on Y axis.  
# 
# ARGS:
# 	List xlists: List of Arrays of X values of curves to be plotted.
# 	List ylists: Corresponding List of Arrays of Y values of curves to be plotted.
# 	String plttype: Type of plot to generate. Only changes labels of axes.
# 	Boolean dots: If True, plot a second time, showing individual points with connecting curves.
#	int maxlength: Only plot up to a finite number of points per curve. 
# RETURNS:
#	None
def plot_semilog(xlists,ylists,plttype = "fft",dots = False,maxlength = 50000):
	for i in range(len(xlists)):
		if maxlength<xlists[i].size:
			plt.semilogy(xlists[i][::xlists[i].size//maxlength],ylists[i][::xlists[i].size//maxlength])
		else: 
			plt.semilogy(xlists[i],ylists[i],label = "Harmonic"+str(i+1)) 
	print(str(i+1)+" Curves Plotted")
	# Show constant values used during generation of this data.
	const.printout()
	if plttype == "fft":
		plt.xlabel("Frequency f")
		plt.ylabel("Amplitude")
		plt.title("FFT of R = "+str(const.R)+" cm filtered data")
	elif plttype == "harmonics":
		plt.xlabel("Frequency f")
		plt.ylabel("Amplitude of harmonic")
		plt.title("Generation of harmonics through Tracing Error")
		plt.legend()
	if dots:
		for i in range(len(xlists)):
			if maxlength<xlists[i].size:
				plt.plot(xlists[i][:maxlength],ylists[i][:maxlength],"o")
			else: 
				plt.plot(xlists[i],ylists[i],"o")
	plt.show()

# Plot a list of several curves. If NaN values are detected, report them, but plot others.
# 
# ARGS:
# 	List xlists: List of Arrays of X values of curves to be plotted.
# 	List ylists: Corresponding List of Arrays of Y values of curves to be plotted.
# 	Boolean dots: If True, plot a second time, showing individual points with connecting curves.
# RETURNS:
#	None
def plot_fourier(xlists,ylists,failures,dots = False):
	fail = failures
	for i in range(len(xlists)):
		plt.plot(xlists[i],ylists[i]) 
	print(str(i+1)+" Curves Plotted")
	if fail>0:
		print(str(fail)+" Curves Failed")
	# Show constant values used during generation of this data.
	const.printout()
	plt.xlabel("Scaled X Position")
	plt.ylabel("Translated Y Position")
	plt.title("Scaled Fourier Series Approximations of Tracing Distortion")
	if dots:
		for i in range(len(xlists)):
			plt.plot(xlists[i],ylists[i],"o")
	plt.show()

if __name__ == "__main__":
	pass

