# This file is the main point of the project. From the command line, 
# you select the function to call, and add parameters. Other than integrating together
# functions from the respective constituent files, as little computation and 
# mathematics as possible is implemented here.

from modeling import processing,sampleinputs,constants,springs,fourierprocessing,freqspacefilter
from wavinterfaces import readwav,writewav
from output import displayfilter
import sys
import numpy as np

# Create and return a shared object containing modifiable constants, 
# and link all files to this object. All other functions in main.py take the 
# const object as an argument. After this is executed, all modules agree with 
# instantaneous state of constants used.
#
# ARGS:
#	None
# RETURNS:
# 	constants.Constants() object.
def set_all_constants():
	constObj = constants.Constants()
	processing.set_constants(constObj)
	sampleinputs.set_constants(constObj)
	springs.set_constants(constObj)
	fourierprocessing.set_constants(constObj)
	freqspacefilter.set_constants(constObj)
	readwav.set_constants(constObj)
	writewav.set_constants(constObj)
	displayfilter.set_constants(constObj)
	return constObj

# Apply finite ball filter to file in shared directory with main.py.
#
# ARGS:
#	 String filein: String of input file name.
#	 String fileout: String of output file name. If filein == fileout, overwrite filein.
#	 Constants const: constants.Constants() object.
# RETURNS:
#	None
def filter_data(filein,fileout,const):
	print("reading file...")
	dys = readwav.readfile(filein)
	print("file length: "+str(dys.size)+" points")
	print("integrating...")
	ys = processing.integrate(dys)
	xs = processing.getx(ys)
	print("processing vectors...")
	gx,gy = processing.gamma(ys)
	print("condensing to single-valued curve...")
	cx,cy = processing.condense(xs,gx,gy)
	dcy = processing.ddx(cy)
	print("writing output file...")
	writewav.writefile(fileout,dcy)

# Apply finite ball filter to one of the default files in WAVstuff/WAVfiles.
# Then display image in matplotlib. What to display is passed as arg. 
#
# ARGS:
#	String filein: String of input file name.
# 	String fileout: String of output file name. If filein == fileout, overwrite filein.
# 	Constants const: constants.Constants() object.
# 	int output: Which plot to display. 
# 		1: Show input, normal vector, poid in time domain, translated directly onto one another.
# 		2: Show derivative of input, derivative of poid in time domain.
# 		3: Show fft of derivative of input, fft of derivative of poid in frequency domain.
#	Boolean write_to_wav: If False, do not output a .WAV file
# RETURNS:
#	None
def view_filtered_data(filein,fileout,const,output = 3,write_to_wav = True,labels = None):

	print("reading file...")
	dys = readwav.readfile(filein)
	print("file length: "+str(dys.size)+" points")
	print("integrating...")
	ys = processing.integrate(dys)
	xs = processing.getx(ys)
	print("processing vectors...")
	gx,gy = processing.gamma(ys)
	print("condensing to single-valued curve...")
	cx,cy = processing.condense(xs,gx,gy)
	dcy = processing.ddx(cy)
	xlists = [xs,gx,cx]
	ylists = [ys,gy-const.R,cy-const.R]
	xlists2 = [xs,cx]
	ylists2 = [dys,dcy]
	if write_to_wav:
		print("writing output file...")
		writewav.writefile(fileout,dcy)
	print("computing FFT...")
	fs1,dYs = fourierprocessing.takefft(dys,200000)
	fs2,dCYs = fourierprocessing.takefft(dcy,200000)
	xlists3 = [fs1,fs2]
	ylists3 = [np.abs(dYs),np.abs(dCYs)]
	print("displaying images...")
	if output == 1:
		displayfilter.plot_curve(xlists,ylists,dots = False,labels = labels)
	elif output == 2:
		displayfilter.plot_curve(xlists2,ylists2,dots = False,labels = labels)
	elif output == 3:
		displayfilter.plot_log(xlists3,ylists3,maxlength = 200000,dots = False,labels = labels)

# Apply ball filter to an example sine wave.
#
# ARGS:
#	 Constants const: constants.Constants() object.
# RETURNS:
#	None
def view_example_sin(const):
	const.set_radius(.06)
	print("creating input waveform...")
	xs,ys = sampleinputs.sinwave(100,10)
	print("processing vectors...")
	gx,gy = processing.gamma(ys)
	print("condensing to single-valued curve...")
	cx,cy = processing.condense(xs,gx,gy)
	xlists = [xs,gx,cx]
	ylists = [ys,gy,cy]
	print("displaying images...")
	displayfilter.plot_curve(xlists,ylists,dots = True)

# Display results of generating a finitely ordered fourier series from a basic sine wave input.
#
# ARGS:
#	float lastfreq: Highest frequency displayed in output.
#	int steps: Number of frequencies calculation is performed on. Resolution = steps/lastfreq.
# 	Constants const: constants.Constants() object.
# 	int order: Number of terms of series generated.
# 	Boolean scaled: If True, multiply each term by the associated frequency, to undo effect of integration of input sine.
# 	Boolean diff: If True, show derivative of cosine series.
# RETURNS:
#	None
def fourier_series(lastfreq,steps,const,order=50,scaled = True,diff = False):
	if scaled:
		h,f = freqspacefilter.view_harmonics(const.A,const.R,lastfreq,steps+1,order,retf = scaled)
		h*=np.repeat(f[1:],order,0).reshape(steps,order)
	else: 
		h = freqspacefilter.view_harmonics(const.A,const.R,lastfreq,steps+1,order,retf = scaled)
	x = np.linspace(-np.pi,np.pi,10000)
	dx = np.diff(x)[0]
	xs = []
	ys = []
	ydiffs = []
	failures = 0
	for i in range(steps):
		F = freqspacefilter.fourier_function(h[i])
		if not np.any(np.diff(F(x[1000:2000]))>0):
			xs += [x]
			ys += [F(x)]
			ydiffs+=[processing.ddx(F(x),dx=dx)]
		else:
			failures+=1
	if diff:
		displayfilter.plot_fourier(xs,ydiffs,failures)
	else:
		displayfilter.plot_fourier(xs,freqspacefilter.equalize_starts(ys),failures)

# Display the magnitude of the first few harmonics of tracing distorted sound, as a function of frequency. 
#
# ARGS:
# 	float lastfreq: Highest frequency displayed in output.
# 	int steps: Number of frequencies calculation is performed on. Resolution = steps/lastfreq.
# 	Constants const: constants.Constants() object.
# 	int order: Number of harmonics plotted.
# 	int output: How plot should be displayed. 
# 		1: Normally scaled.
# 		2: Logarithmically scaled.
#	Boolean constvel: If true, scale amplitudes for constant f*a, otherwise do not. 
# RETURNS:
#	None
def harmonics(lastfreq,steps,const,order=3,output = 2,constvel = True):
	h,f = freqspacefilter.view_harmonics(const.A,const.R,lastfreq,steps,order,retf = True,constvel = constvel)
	f = f[1:]
	if output == 1:
		displayfilter.plot_curve(np.transpose(np.repeat(f,order,0).reshape(steps-1,order)),np.transpose(np.abs(h)))
	elif output == 2:
		displayfilter.plot_semilog(np.transpose(np.repeat(f,order,0).reshape(steps-1,order)),np.transpose(np.abs(h)),plttype = "harmonics")


# Display effects of applying a cantilever resonance effect to various input shapes. 
#
# ARGS:
#	String shape: What sample waveform to act upon:
# 		"square": One cycle of a square wave.
# 		"sin": A sine wave.
# 		"spike": An extreme single point discontinuity.
# 		"wav": One of the files already loaded.
# 	Constants const: constants.Constants() object.
# RETURNS:
#	None
def springs_eff(shape,const,diff = False):
	if shape == "square":
		xs,ys = sampleinputs.square(1,1,3)
		ys2 = ys + const.R
		displacedys = springs.apply_spring_effects(ys2)
	elif shape == "sin":
		xs, ys = sampleinputs.sinwave(100,1)
		displacedys = springs.apply_spring_effects(ys)
		diffys = processing.ddx(displacedys)
	elif shape == "spike":
		xs, ys = sampleinputs.spike(.01,1)
		displacedys = springs.apply_spring_effects(ys)
	elif shape == "wav":
		inputlist=["WAVstuff/WAVfiles/4-1-14 French Alphabet 2_-80_1800_6_18000_2.00_2_1.pri1-0-BN100-E0.2-BK100-FL200-BL4-OUT0.4-5XXX-D1-36000-18000.wav"]
		ys = processing.integrate(readwav.readfile(inputlist[0]))
		xs = processing.getx(ys)
		displacedys = springs.apply_spring_effects(ys)
	if diff:
		diffys = processing.ddx(displacedys)
		displayfilter.plot_curve([xs,xs],[ys,diffys])
	else:
		displayfilter.plot_curve([xs,xs],[ys,displacedys])


# This is the main sequence
print()
todo = sys.argv[1]
const = set_all_constants()
# View example of operation
if todo == "apply_sample":
	# example files, already stored in known folder
	inputlist = []
	inputlist+=["4-1-14 French Alphabet 2_-80_1800_6_18000_2.00_2_1.pri1-0-BN100-E0.2-BK100-FL200-BL4-OUT0.4-5XXX-D1-36000-18000.wav"]
	inputlist+=["0279-full_-90_1800_21_18000_1400_4.pri0-0-BN180-E0.2-BK100-FL200-OUT1-1-D1-44100-18000.wav"]
	inputlist+=["4235from-35to-19a__-35_1700_9_48000_3.00_4_1.pri0-0-BN100-E0.2-BK100-FL200-BL4-OUT0.4-51-D1-96000-48000.wav"]
	inputlist+=["can't_say-new_-67_1810_47_18000_800.pri1-0-BN30-E0.2-BK100-BL12-OUT1-1-D1-44100.wav"]
	inputlist+=["DC-0279-full_-90_1800_21_18000_1400_4.pri0-0-BN180-E0.2-BK100-FL200-OUT1-1-D1-44100-18000.wav"]
	inputlist+=["Good_Bye_Tosti_-59_1810_43_18000_800.pri5-0-BN30-E0.2-BK100-BL8-OUT1-1-D1-44100.wav"]
	inputlist+=["Rakoczy_-95_1810_52_18000_800.pri1-0-BN30-E0.2-BK100-BL12-OUT1-1-D1-44100.wav"]
	inputfilename = inputlist[int(sys.argv[2])]
	# Add optional parameters
	if len(sys.argv)>3:
		const.set_radius(float(sys.argv[3]))
	if len(sys.argv) > 4:
		out = int(sys.argv[4])
	else:
		out = 3
	labels = None
	view_filtered_data("WAVstuff/WAVfiles/"+inputfilename,"WAVstuff/outputWAVfiles/"+inputfilename[:-4]+"-filtered.wav",const,output = out,labels = labels)
# Apply filter to any .wav file, with name entered manually. Output is stored at same location, with "-filtered" appended on to title.
elif todo == "apply_filter":
	inputfilename = sys.argv[2]
	if len(sys.argv)>3:
		const.set_radius(float(sys.argv[3]))
	if len(sys.argv) > 4 and (sys.argv[4] == "--overwrite" or sys.argv[4] == "-o"):
		outname = inputfilename
	else:
		outname = inputfilename[:-4]+"-filtered.wav"
	filter_data(inputfilename,outname,const)
# Display a finite fourier cosine approximation to tracing error effects.
elif todo == "fourier_series":
	const.set_radius(.2)
	lastfreq = float(sys.argv[2])
	steps = int(sys.argv[3])
	order = 50
	diff = False
	if len(sys.argv)>4:
		order = int(sys.argv[4])
	if len(sys.argv)>5:
		diff = True
	fourier_series(lastfreq,steps,const,order,diff = diff)
# Display the evolution of fourier cosine coefficients with frequency.
elif todo == "harmonics":
	const.set_radius(9.5e-3)
	lastfreq = float(sys.argv[2])
	steps = int(sys.argv[3])
	order = 3
	if len(sys.argv)>4:
		order = int(sys.argv[4])
	harmonics(lastfreq,steps,const,order,constvel = False)
# Display the effect created by applying a cantilever resonance effect to the shape. 
elif todo == "see_springs":
	shape = sys.argv[2]
	if len(sys.argv)>3:
		f = sys.argv[3]
		tau = sys.argv[4]
		prop = sys.argv[5]
		const.set_spring_constants(f,tau,prop)
	springs_eff(shape,const)
# For generating sample images, see ghosts_of_temp.py for image code. Empty now.
elif todo == "temp":
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	const.set_radius(0.001)
	const.set_wav_constants(10000)
	xs,ys1 = sampleinputs.sinwave(100,.03/const.dx)
	ys2 = -.2*sampleinputs.sinwave(150,.03/const.dx)[1]
	ys3 = .6*sampleinputs.sinwave(20,.03/const.dx)[1]
	ys = 2*(ys1+ys2+ys3)+2
	gammax,gammay = processing.gamma(ys)
	cx,cy = processing.condense(xs,gammax,gammay)
	x1 = .00764631/const.dx
	y1 = np.interp(np.array([x1]),cx,cy)[0]
	x2 = .0122545/const.dx
	y2 = np.interp(np.array([x2]),cx,cy)[0]
	x3 = .00410829/const.dx
	y3 = np.interp(np.array([x3]),cx,cy)[0]
	circ1x,circ1y = sampleinputs.circle(x1,y1,const.R)
	circ2x,circ2y = sampleinputs.circle(x2,y2,const.R)
	circ3x,circ3y = sampleinputs.circle(x3,y3,const.R)
	ax.plot(circ1x,circ1y,'g',label = "_nolabel_")
	ax.plot(circ2x,circ2y,'g',label = "_nolabel_")
	ax.plot(circ3x,circ3y,'g',label = "_nolabel_")
	ax.set_xlabel("Direction of recording (cm)")
	ax.set_ylabel("Modulation Height (cm)")
	line1 = ax.plot(np.array([x1,x2,x3]),np.array([y1,y2,y3]),"go",label = "Needle Positions")
	ysstar = ys + const.R
	line2 = ax.plot(xs,cy,'-',label = "Poid")
	line3 = ax.plot(xs,ysstar,'r-.',label = "Undistorted Playback")
	line4 = ax.fill_between(xs,ys,label = "Surface")
	ax.set_title("Example of Tracing Distortion")
	ax.axis([0.000576445,.022019,1.99543,2.01321])
	ax.legend()
	plt.show()
else: 
	print('Your selection "'+todo+'" is not a valid command.')
print()
print()

