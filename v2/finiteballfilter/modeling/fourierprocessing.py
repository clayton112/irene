# This file performs most of the explicit processing of FFTs and processing of 
# input files into FFTs.

import numpy.fft as fft
import numpy as np

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj


# Perform an FFT of input data.
#
# ARGS:
# 	Array ys: Input points in time domain.
# 	int maxlength: FFTs can be prohibitively slow without much increase in value 
#		for large waveforms, so process only some at a time, and average the results.
# 	float dt: The TIME difference between points (NOT space!). If dt not given, assume from const.
# RETURNS:
#	Array fs: Frequencies associated with FFT bins, size = min(maxlength//2+1,ys.size//2+1)
#	Array Ys: (Complex) amplitudes associated with each frequency bin, size = min(maxlength//2+1,ys.size//2+1)
def takefft(ys,maxlength=200000,dt = None):
	if not dt:
		dt = 1.0/const.SAMPLERATE
	if ys.size>maxlength:
		Ys = []	
		fs = fft.rfftfreq(maxlength, d= dt)
		ysary = np.split(ys[:-(ys.size % maxlength)],ys.size//maxlength)
		for ylst in ysary:
			Ys += [fft.rfft(ylst)]
		# The last element of Ys is likely to not be the same size, ignoring it will not be a problem 
		# unless all important dynamics happen in the last 4 seconds or so.
		return fs,np.average(np.array(Ys[:-1]),axis = 0)
	else:
		fs = fft.rfftfreq(ys.size,d = dt)
		Ys = fft.rfft(ys)
		return fs,Ys

# Find the magnitude of the closest frequency bin to a given frequency in an FFT.
# May be inaccurate if resolution too low!
#
# ARGS:
# 	float f: Frequency to search for.
# 	Array Ys: FFT of data.
# 	float dt: Time difference between samples the FFT was formed with.
# 	Boolean absval: If True, take the absolute value of FFT at f.
# RETURNS:
#	float Y: Magnitude of appropriate frequency bin.
def frequency_component(f,Ys,dt,absval = True):
	Nn = Ys.size
	if absval:
		return np.abs(Ys[int(Nn*f*dt/np.pi)])
	else:
		return Ys[int(Nn*f*dt/np.pi)]

def fix_discontinuities(mags):
	numtimes,size = mags.shape
	for i in range(numtimes):
		for j in range(1,size-1):
			if mags[i,j] < mags[i,j-1] and mags[i,j] < mags[i,j+1]:
				mags[i,j] = .5*mags[i,j-1]+.5*mags[i,j+1]
			elif mags[i,j] > mags[i,j-1] and mags[i,j] > mags[i,j+1]:
				mags[i,j] = .5*mags[i,j-1]+.5*mags[i,j+1]
	return mags


if __name__ == "__main__":
	pass
	






