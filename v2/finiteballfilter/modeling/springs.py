# This file can apply the effects of cantilever vibration to the already generated path of the 
# moving stylus. It does NOT represent additional perturbation of the stylus position, which
# we here will assume is a much smaller effect, but merely the effect on the overall sound 
# from the mechanical nature (what we will here simulate) of a magnetic pickup.

import numpy as np

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj



# Calculate effect of cantilever oscillation on an input waveform.
# 
# ARGS:
#	Array amps: All impulses experienced by stylus.
#	float decay: Oscillations will be maintained until they decay to e^(-decay) 
#		their original strength, then assumed 0. 
# RETURNS:
#	Array effects: All effects of springs, to be added onto original waveform.
def spring_effects(amps,decay):
	numsprings = decay*const.tau//const.dx
	springs = np.zeros(numsprings)
	t = np.flipud(np.linspace(0.0,decay*const.tau,numsprings))
	effects = np.zeros(amps.size+1)
	for i in range(1,amps.size+1):
		low = max(0,i-numsprings)
		high = -min(i,numsprings)
		effect = (const.k*np.sum(amps[low:i]
			*np.sin(const.w0*t[high:])*np.exp(-t[high:]/const.tau)))
		effects[i] = effect
	return effects

# Apply effect of cantilever oscillation to an input waveform.
# 
# ARGS:
#	Array cy: y-positions of original waveform.
#	float decay: Oscillations will be maintained until they decay to e^(-decay) 
#		their original strength, then assumed 0. 
# RETURNS:
#	Array ncy: Perturbed waveform.
def apply_spring_effects(cy,decay=5.0):
	amplitudes = np.diff(cy)
	toAdd = spring_effects(amplitudes,decay)
	return cy+toAdd

if __name__ == "__main__":
	pass
