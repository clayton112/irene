# This file is used to generate quick sample inputs for use testing the other files.
# Many tests have been removed from this version, so now it is mostly referenced in 
# main.springs_eff()

import numpy as np 

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj


# Create sine wave, scaled to 1.
# 
# ARGS:
# 	float freq: Frequency of sine wave.
# 	float length: Length of output curve in space.
#	float start: Starting position of curve in x.
#	Boolean scaled: If True, amplitude of wave is found in const.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def sinwave(freq,length,start = 0.0,scaled = True):
	xs = np.arange(start,start+length,const.dx)
	ys = np.sin(2*np.pi*freq*xs/const.v)
	if scaled:
		ys *= const.A
	return xs,ys

# Create cosine wave, scaled to 1.
# 
# ARGS:
# 	float freq: Frequency of cosine wave.
# 	float length: Length of output curve in space.
#	float start: Starting position of curve in x.
#	Boolean scaled: If True, amplitude of wave is found in const.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def coswave(freq,length,start = 0.0,scaled = True):
	xs = np.arange(start,start+length,const.dx)
	ys = np.cos(2*np.pi*freq*xs/const.v)
	if scaled:
		ys *= const.A
	return xs,ys

# Create a line of constant slope
# 
# ARGS:
# 	float slope: Slope of line.
# 	float length: Length of output curve in space.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def line(slope,length):
	xs = np.arange(0,length,const.dx)
	ys = np.arange(0,length*slope,const.dx*slope)
	return xs,ys

# Create a waveform that is zero before some point, 1 after.
# 
# ARGS:
# 	float time: Point in space where value changes.
# 	float length: Length of output curve in space.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def shelf(time,length):
	xs = np.arange(0,length,const.dx)
	ys = (np.sign(xs-time)+1)/2
	return xs,ys

# Create a waveform that is zero before some point, 1 after, until some time passes, and then 0 again.
# 
# ARGS:
# 	float time: point in space where value changes to 1.
# 	float width: point in space where value changes back to 0.
# 	float length: Length of output curve in space.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def square(time,width,length):
	if time + width > length:
		raise ValueError("time too short!")
	xs,ys1 = shelf(time,length)
	ys2 = 1-shelf(time+width,length)[1]
	return xs,ys1+ys2-1

# Create a waveform that is zero before some point, 1 at that single point, and 0 after.
# 
# ARGS:
# 	float time: point in space where value changes to 1.
# 	float length: Length of output curve in space.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def spike(time, length):
	xs = np.arange(0,length,const.dx)
	ys = np.zeros(xs.size)
	ys[time//const.dx] = 1
	return xs,ys

# Create a circle of radius r from some center. 
# Used for scaling of axes and comparison, NOT as an input waveform.
# 
# ARGS:
# 	float centerx: X position of center.
# 	float centery: Y position of center.
# 	float radius: Radius of ball.
# RETURNS:
#	Array xs: X positions of shape.
#	Array ys: Y positions of shape.
def circle(centerx,centery,radius):
	ts = np.linspace(0,2*np.pi,1000)
	xs = centerx + radius*np.cos(ts)
	ys = centery + radius*np.sin(ts)
	return xs,ys

# Create simple tether between two curves, to see point-by-point distortion effects created in processing.
# 
# ARGS:
# 	Array origx: X positions of unprocessed waveform.
# 	Array origy: Y positions of unprocessed waveform.
# 	Array processx: X positions of processed waveform.
# 	Array processy: Y positions of processed waveform.
# RETURNS:
#	List xlist: List of X position arrays of tether (simple arrays, two points each).
#	List ylist: List of Y position arrays of tether (simple arrays, two points each).
def show_norms(origx,origy,processx,processy,pos = 0,num = 20):
	vals = np.arange(pos,pos+num)
	xlist = []
	ylist = []
	for i in vals:
		xlist+=[np.array([origx[i],processx[i]])]
		ylist+=[np.array([origy[i],processy[i]])]
	return xlist,ylist


if __name__ == "__main__":
	pass