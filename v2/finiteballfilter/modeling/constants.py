# Here we save a number of variables which will need to be referenced and shared among many files.
# These are expected to be constants with respect to any single data collection program, but need 
# to be modifiable at initialization. All of the other .py files reference a global variable known
# as "const" which contains all of these constants, and is linked at initialization.

from numpy import pi

class Constants:

	# Fixed constants, predetermined by shape.
	CYLINDER_SIZE = 5.7;  # Diameter in cm.
	VELOCITY = 120 * 1/60 * pi * CYLINDER_SIZE; # =35.28; Linear speed of groove in ROT/MIN*MIN/sec*cm/ROT = cm/sec.

	# Semi-dynamic constants, central to analysis, but constant with respect to each experiment.
	MAX_HEIGHT_MODULATION = 30E-4 # 30 microns, in cm.
	SAMPLERATE = 44100.0 # Sampling rate of data points, in Hz. 
	CMPERPT = VELOCITY/SAMPLERATE # = .000812; Distance between sampled points, in cm.
	RADIUS = 9.5e-3 # Radius of stylus, in cm.

	# Cantilever resonance constants.
	OMEGA = 2*pi*10000 # Natural frequency of cantilever, in rad/sec.
	TAU = .1 # Natural decay time of cantilever oscillations, in seconds.
	PROPORTIONALITY = .2 # Proportionality of cantilever resonance effects to stylus effects, unitless.

	# Defunct processing aid constants.
	POINTSPERPOINT = 10 # Used for upsampling of data.
	MAXLENGTH = 2**62 # Number of points to process at once.
	 
	# Shorter versions of previously defined constants for clarity and readability in other files.
	A = MAX_HEIGHT_MODULATION
	dx = CMPERPT
	v = VELOCITY
	dt = dx/v
	R = RADIUS
	w0 = OMEGA
	tau = TAU
	k = PROPORTIONALITY

	# Very straightforward setting methods.
	def set_granularity(self,n):
		self.POINTSPERPOINT = n
	def set_spring_constants(self,f,tau,prop):
		self.OMEGA = 2*pi*f
		self.w0 = 2*pi*f
		self.TAU = tau
		self.tau = tau
		self.PROPORTIONALITY = prop
		self.k = prop
	def set_radius(self,r):
		self.RADIUS = r
		self.R = self.RADIUS
	def set_dx(self,dx):
		f = self.v/dx
		self.set_wav_constants(f)
	def set_wav_constants(self,f):
		self.SAMPLERATE = f
		self.CMPERPT = self.VELOCITY / self.SAMPLERATE
		self.dx = self.CMPERPT
		self.dt = self.dx / self.v
	def set_height(self,height):
		self.MAX_HEIGHT_MODULATION = height
		self.A = height
	def set_maxlength(self,maxlen):
		self.MAXLENGTH = maxlen

	# Display all relevant physical constants, for use debugging and interpreting data.
	def printout(self):
		print("CYLINDER_SIZE: "+str(self.CYLINDER_SIZE)+" cm")
		print("LINEAR VELOCITY: "+str(self.VELOCITY)+" cm/sec")
		print("HEIGHT SCALE FACTOR: "+str(self.MAX_HEIGHT_MODULATION)+" cm")
		print("SAMPLERATE: "+str(self.SAMPLERATE)+" pts/sec")
		print("CMPERPT: "+str(self.CMPERPT)+" cm/pt")
		print("R = "+str(self.RADIUS)+" cm")
	


