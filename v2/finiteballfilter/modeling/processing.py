# This file implements time-domain operations to be applied to general sampled waveforms.

import numpy as np
from scipy.integrate import cumtrapz

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj



# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Array y: Evenly-spaced sample points to be matched by x values.
#	float start: Starting position for values, default 0.
# RETURNS:
#	Array x: Evenly-spaced x values. 
def getx(y,start = 0.0):
	return np.linspace(start,start+y.size*const.dx,y.size,endpoint = False)

# Take a numerical derivative of y values. This is a second-order finite difference formula.
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
#	float dx: Spacing between points. If not given, assume from const.
# RETURNS:
#	Array yprime: Derivative of y.
def ddx(y,dx = None):
	if not dx:
		deltax = const.dx
	else:
		deltax = dx
	return np.gradient(y,deltax)

# Numerically integrate y values. This uses the composite trapezoidal rule.
# We assume that the integral of Y at 0 is zero. This does not have any negative consequences, 
# because all final conclusions are drawn from derivatives of this integrated form, which will
# disregard constant additions.
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
#	float dx: Spacing between points. If not given, assume from const.
# RETURNS:
#	Array Y: Cumulative integral of Y. 
def integrate(y,dx = None):
	if not dx:
		deltax = const.dx
	else:
		deltax = dx	
	return cumtrapz(y,initial = 0, dx = deltax)

# Return the x position of the normal vector addition to x values. The x values are not needed here.
# Rx is computed using only y positions and Constants object.
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
# RETURNS:
#	Array rx: X component of normal vector.
def rx(y):
	yprime = ddx(y)
	return -const.R*yprime/(yprime**2+1)**.5

# Return the y position of the normal vector addition to y values.
# Ry is computed using only y positions and Constants object.
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
# RETURNS:
#	Array ry: Y component of normal vector.
def ry(y):
	yprime = ddx(y)
	return const.R/(yprime**2+1)**.5

# Compute x coordinate of normal curve to y points. 
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
#	float start: Starting position for values, default 0.
# RETURNS:
#	Array gx: X component of normal curve.
def gammax(y,start = 0.0):
	x = getx(y,start)
	return x + rx(y)

# Compute y coordinate of normal curve to y points. 
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
# RETURNS:
#	Array gy: Y component of normal curve.
def gammay(y):
	return y + ry(y)

# Compute normal curve to y points. 
# 
# ARGS:
#	Array y: Evenly-spaced sample points.
#	float start: Starting position for values, default 0.
# RETURNS:
#	Array gx: X component of normal curve.
#	Array gy: Y component of normal curve.
def gamma(y,start = 0.0):
	return gammax(y,start),gammay(y)

# Copy two numpy arrays into two larger, partially filled arrays, starting at a given position.
# 
# ARGS:
#	Array listx: X coordinates of array to be written into.
#	Array listy: Y coordinates of array to be written into.
#	int start: Position in listx,listy to begin copying elements.
#	Array ux: X coordinates to copy.
#	Array uy: Y coordinates to copy.
# RETURNS:
#	None
def copyin(listx,listy,start,ux,uy):
	for i in range(ux.size):
		listx[start+i] = ux[i]
		listy[start+i] = uy[i]

# Iterate backwards to find the first element of an array before a given point.
# If not found, return negative index of zeroth position
# 
# ARGS:
#	Array listx: Array of increasing points.
#	float x: Point to be searched for.
# RETURNS:
#	int i: (Negative) index of first element less than x. 
def countback(listx,x):
	i=-1
	while -i<listx.size and listx[i]>x:
		i-=1
	return i

# Iterate forwards to find the first element of an array after a given point.
# If not found, return index of -1th position
# 
# ARGS:
#	Array listx: Array of increasing points.
#	float x: Point to be searched for.
# RETURNS:
#	int i: Index of first element greater than x. 
def countforward(listx,x):
	i=0
	while i<listx.size and listx[i]<x:
		i+=1
	return i

# Interpolate a two dimensional sample onto a given set of x values.
# 
# ARGS:
#	Array nxs: The x positions of the desired interpolated points.
#	Array xs: The x positions of the current calculated points.
#	Array ys: The y positions of the current calculated points.
# RETURNS:
#	Array nxs: The x positions of the desired interpolated points.
#	Array nys: The interpolated y positions.
def even_spaced_interpolate(nxs,xs,ys):
	nys = np.interp(nxs,xs,ys)
	return nxs,nys

# Determine whether two line segments (represented by two 2D points each) intersect.
# 
# ARGS:
#	float a1x: X component of 1st point of 1st line.
#	float a1y: Y component of 1st point of 1st line.
#	float a2x: X component of 2nd point of 1st line.
#	float a2y: Y component of 2nd point of 1st line.
#	float b1x: X component of 1st point of 2nd line.
#	float b1y: Y component of 1st point of 2nd line.
#	float b2x: X component of 2nd point of 2nd line.
#	float b2y: Y component of 2nd point of 2nd line.
# RETURNS:
#	Boolean cross: True if line segments do intersect.
#	float x: X position of intersection. -1 if none.
#	float y: Y position of intersection. -1 if none.
def lines_intersect(a1x,a1y,a2x,a2y,b1x,b1y,b2x,b2y):
	if a1x == a2x or b1x == b2x:
		return False,-1,-1
	am = (a2y-a1y)/(a2x-a1x)
	bm = (b2y-b1y)/(b2x-b1x)
	if am==bm:
		return False,-1,-1
	x = (-a1y+b1y+am*a1x-bm*b1x)/(am-bm)
	y = am*(x-a1x)+a1y
	return a1x<x<a2x and b1x<x<b2x,x,y

# Determine whether two finite arrays have any intersecting line segments.
# If so, return an array of their intersection points, else return an empty array.
# Returns arrays because they are immediately concatenated, with an empty concatenation
# having no effect.
# 
# ARGS:
#	Array ux: X positions of 1st curve.
#	Array ux: Y positions of 1st curve.
#	Array vx: X positions of 2nd curve.
#	Array vy: Y positions of 2nd curve.
# RETURNS:
#	Array x: One-element array of x position of the curves' intersection.
#	Array y: One-element array of y position of the curves' intersection.
def curves_intersect(ux,uy,vx,vy):
	for i in range(ux.size-1):
		for j in range(vx.size-1):
			intersects,x,y = lines_intersect(ux[i],uy[i],ux[i+1],uy[i+1],vx[j],vy[j],vx[j+1],vy[j+1])
			if intersects:
				return np.array([x]),np.array([y])
	return np.array([]),np.array([])

# Find intersection of two curves, if it exists. Then, take all elements of first curve
# BEFORE intersection, the intersection itself, and all elements of second curve AFTER intersection.
# If no intersection is found, return an empty array.
# 
# ARGS:
#	Array ux: X positions of 1st curve.
#	Array ux: Y positions of 1st curve.
#	Array vx: X positions of 2nd curve.
#	Array vy: Y positions of 2nd curve.
# RETURNS:
#	Array uvx: X positions of combined array, empty if no intersection.
#	Array uvy: Y positions of combined array, empty if no intersection.
def take_sections(ux,uy,vx,vy):
	x,y=curves_intersect(ux,uy,vx,vy)
	if x.size==0:
		return x,y
	i = countforward(ux,x[0])
	j = countback(vx,x[0])
	return np.concatenate([ux[:i+1],x,vx[j:]]),np.concatenate([uy[:i+1],y,vy[j:]])

# Crop curves passed to condense() such that the loop in condense() begins correctly (x increasing).
# 
# ARGS:
#	Array gammax: X positions of curve to crop.
#	Array gammay: Y positions of curve to crop.
# RETURNS:
#	Array gammax: X positions of cropped curve.
#	Array gammay: Y positions of cropped curve.	
def fix_start(gammax,gammay):
	d = np.diff(gammax)
	i=0
	while d[i]<0:
		i+=1
	return gammax[i:],gammay[i:]

# Simple counting class, for readability and minimal variables used in condense().
# ATTRIBUTES:
# 	int count: Current position in input array.
# 	int ret_count: Current position in output array.
#	int end: Final position in output array.
#	int i: Percent complete (for printing progress bar).
#	boolean not_done: If False, iteration is finished.
class Counter:
	count = 0
	ret_count = 0
	endpoint = 0
	i=0
	not_done = True
	# Initialized with end value
	def __init__(self,theend):
		self.endpoint = theend
	# Update count, and update not_done if needed.
	# Also, occasionally print "*" to indicate progress along iteration.
	def count_iter(self):
		self.count+=1
		if 30.0*self.count/self.endpoint>self.i:
			self.i += 1
			print("*", end = "")
		if self.count >= self.endpoint:
			self.not_done = False
			print("")
	# Update ret_count. Can be positive or negative.
	def ret_count_iter(self,inc):
		self.ret_count+=inc

# This function detects the presence of Type II tracing distortion (read: Normal Vector curve 
# places stylus in impossible or unphysical positions), here represented as 
# self-intersections of the normal curve. If not detected, return interpolated 
# points, designed to correspond exactly with input values. If detected,
# iterate over all points in curve to determine where normal self-intersections
# occur, and keep uppermost branch of all multivalued regions.
# 
# ARGS:
#	Array nxs: Desired x positions of output shape.
#	Array gammax: X positions of normal curve. 
#	Array gammay: Y positions of normal curve. 
# RETURNS:
#	Array nxs: Desired x positions of output shape.
#	Array nys: Interpolated y positions of condensed normal curve at points nxs.
def condense(nxs,gammax,gammay):
	# Check if iteration necessary. If not, simply interpolate.
	if not np.any(np.diff(gammax)<0):
		return even_spaced_interpolate(nxs,gammax,gammay)
	print("Tracing Error Detected! Iterating over points, please wait.")
	print("------------------------------")
	gammax,gammay = fix_start(gammax,gammay)
	end = gammay.size-1
	toReturnx = np.zeros(end)
	toReturny = np.zeros(end)
	c = Counter(end)
	while c.not_done:
		# While x advancing, add all points to output.
		while c.not_done and gammax[c.count+1] >= gammax[c.count]:
			toReturny[c.ret_count]=gammay[c.count]
			toReturnx[c.ret_count]=gammax[c.count]
			c.count_iter()
			c.ret_count_iter(1)
		if not c.not_done:
			break
		# When x stops advancing, find end point that brackets the region of multivaluedness.
		stopPoint = c.count
		# Ignore points iterating backwards. They are necessarily lower branches. 
		while c.not_done and gammax[c.count+1] <= gammax[c.count]:
			c.count_iter()
		if not c.not_done:
			break
		# When x stops receding find beginning point that brackets the region of multivaluedness.
		startPoint = c.count
		# Remove and store previously added points from output within bracketed region.
		usize = -countback(toReturnx[:c.ret_count],gammax[startPoint])-1
		ux = np.copy(toReturnx[c.ret_count-usize:c.ret_count])
		uy = np.copy(toReturny[c.ret_count-usize:c.ret_count])
		c.ret_count_iter(-usize)
		copyin(toReturnx,toReturny,c.ret_count,np.zeros(usize),np.zeros(usize))
		while c.not_done and gammax[c.count]<=gammax[stopPoint]:
			c.count_iter()
		# Store next points of input contained in bracketed region.
		if not c.not_done:
			vx = np.copy(gammax[startPoint:c.count+1])
			vy = np.copy(gammay[startPoint:c.count+1])
		else:
			vx = np.copy(gammax[startPoint:c.count])
			vy = np.copy(gammay[startPoint:c.count])
		# Find intersection of two stored curve, take upper branch of both.
		# If no intersection (see documentation for example), return empty.
		# These points will be linearly interpolated from earlier and later points.
		uvx,uvy = take_sections(ux,uy,vx,vy)
		copyin(toReturnx,toReturny,c.ret_count,uvx,uvy)
		c.ret_count_iter(uvx.size)
	# Interpolate onto desired output.
	toReturnx = np.trim_zeros(toReturnx,trim = "b")
	return even_spaced_interpolate(nxs,toReturnx,toReturny[:toReturnx.size])

if __name__ == "__main__":
	pass
