# Here we apply the general methods developed in processing.py to a special case of a pure 
# sine wave tone. Therefore, we can be much more eetaact, and can have much more in-depth analysis
# in which we will consider the final tracing distortion effect as a Fourier series expansion.

from scipy.optimize import brentq
from scipy.integrate import quad
import numpy as np
import numpy.fft as fft

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj


# Returns amplitude and spacial period for a given frequency, and lets
# constant-velocity recording be an option.
# 
# ARGS:
#	Array f: Frequencies of the surface in question. Also can take a float as an arg.
#	float a: Amplitude of sine waves. If constvel = True, a is the amplitude of the derivative
#		of the surface. Otherwise, a is the amplitude of the surface itself.
#	Boolean constvel: If true, scale amplitudes for constant f*a, otherwise do not. 
# RETURNS:
#	Array A: Amplitudes of constituent waves. A[i] = a if not constvel and f is an Array.
#	Array X: Spacial Periods of constituent waves.
def get_AX(f,a,constvel = True):
	X = const.v / f
	if constvel:
		A = a*.5*X/np.pi
	else:
		A = f/f*a
	return A,X

# Higher-order function that generates 1-variable curve (original shape).
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
# RETURNS:
#	function f(eta): Y coordinate of simulated surface. 
def make_f(A,R,X):
	Xbar = .5*X/np.pi
	def f(eta):
		return -A*np.cos(eta/Xbar)
	return f

# Higher-order function that generates 1-variable curve (eta normal vector addition).
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
# RETURNS:
#	function rx(eta): X coordinate of normal vector addition. 
def make_rx(A,R,X):
	Xbar = .5*X/np.pi
	def rx(eta):
		num = -R*A/Xbar*np.sin(eta/Xbar)
		denom = np.sqrt((A/Xbar*np.sin(eta/Xbar))**2+1)
		return num / denom
	return rx

# Higher-order function that generates 1-variable curve (eta normal vector addition derivative).
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
# RETURNS:
#	function rxprime(eta): Symbolic derivative of rx(eta).
def make_rxprime(A,R,X):
	Xbar = .5*X/np.pi
	def rxprime(eta):
		num = -A/Xbar**2*R*np.cos(eta/Xbar)
		denom = ((A/Xbar*np.sin(eta/Xbar))**2+1)**1.5
		return num / denom
	return rxprime

# Higher-order function that generates 1-variable curve (y normal vector addition).
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
# RETURNS:
#	function ry(eta): X coordinate of normal vector addition.
def make_ry(A,R,X):
	Xbar = .5*X/np.pi
	def ry(eta):
		num = R
		denom = np.sqrt((A/Xbar*np.sin(eta/Xbar))**2+1)
		return num / denom
	return ry

# Higher-order function to generate Fourier Coefficient Functions
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
# RETURNS:
#	function make_F(n): Function that creates 1-variable curve to integrate 
#		to find Fourier series coefficients. See documentation.
def make_Fn(A,R,X):
	Xbar = .5*X/np.pi
	p = make_f(A,R,X)
	rx = make_rx(A,R,X)
	rxprime = make_rxprime(A,R,X)
	ry = make_ry(A,R,X)
	def make_F(n):
		def F(eta):
			return (p(eta)+ry(eta))*np.cos(n/Xbar*(eta+rx(eta)))*(1+rxprime(eta))
		return F
	return make_F

# Find position of first eta value mapping to self-intersection point of normal vector. 
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
# RETURNS:
#	float eta0: First eta position mapping to self-intersection point. 
def get_eta0(A,R,X):
	rx = make_rx(A,R,X)
	rxprime = make_rxprime(A,R,X)
	if 1.0+rxprime(0) < 0.0:
		try:
			high = brentq(lambda eta: 1.0+rxprime(eta),-.5*X,0)
			eta0 = brentq(lambda eta: eta+rx(eta),-.5*X,high)
		except ValueError:
			import matplotlib.pyplot as plt
			xs = np.linspace(-.5*X,.001,10000)
			gprimex = lambda x: 1.0+rxprime(x)
			gx = lambda x: x+rx(x)
			plt.plot(xs,gprimex(xs)/100000)
			plt.plot(xs,gx(xs))
			plt.plot(xs,0*xs)
			plt.plot(0*xs,xs)
			plt.show()

	else:
		eta0 = 0.0
	return eta0

# Generate a finite number of Fourier series coefficients for given input conditions.
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float X: Spatial Period of one cycle of curve.
#	int order: Number of terms to generate. 
# RETURNS:
#	Array h: Array of Fourier series coefficients. 
def fourier_coefficients(A,R,X,order):
	h = np.zeros(order)
	eta0 = get_eta0(A,R,X)
	make_F = make_Fn(A,R,X)
	for n in range(order):
		Fn = make_F(n+1)
		h[n] = 4.0/X*quad(Fn,-.5*X,eta0,epsabs = 1e-13)[0]
	return h

# Generate a finite number of Fourier series coefficients for 
# given input conditions at several different frequencies.
# 
# ARGS:
#	float A: Amplitude of final modulations.
#	float R: Radius of stylus.
#	float lastfreq: Highest frequency analyzed.
#	int steps: Number of frequencies analyzed.
#	int order: Number of terms to generate at each frequency. 
#	Boolean retf: If True, return frequencies and Fourier coefficients.
#		If False, only return Fourier coefficients. 
#	Boolean constvel: If true, scale amplitudes for constant f*a, otherwise do not. 
# RETURNS:
#	Array h: 2D Array of Fourier coefficients at different frequencies. h.shape = (steps-1,order)
#	Array f: 1D Array of frequencies associated with h. Only returned if retf == True.
def view_harmonics(amplitude,R,lastfreq,steps,order,retf = False,constvel = True):
	f = np.linspace(0,lastfreq,steps,endpoint = False)
	A,X = get_AX(f,amplitude,constvel)
	h = np.zeros((steps-1,order))
	for i in range(steps-1):
		h[i] = fourier_coefficients(A[i+1],R,X[i+1],order)
	if retf:
		return h,f
	return h

# Display Fourier coefficients at their associated frequencies.
# 
# ARGS:
#	Array h: 2D Array of Fourier coefficients at different frequencies.
#	float lastfreq: Highest frequency analyzed.
# RETURNS:
#	None
def print_h(h,lastfreq):
	steps,order = h.shape
	f = np.linspace(0,lastfreq,steps+1,endpoint = False)
	for i in range(steps):
		print("i="+str(i),end = "")
		print("f = "+str(f[i+1]),end = "")
		print("  Series coefficients: ")
		print(h[i])

# Higher order function to create one-variable Fourier series from coefficients.
# 
# ARGS:
#	Array h: 2D Array of Fourier coefficients at different frequencies.
#	float tolerance: The largest possible Fourier coefficient before the coefficient can be 
#		assumed to be an error. Cause of these errors is UNDETERMINED!
# RETURNS:
#	function f(x): Finite-ordered Fourier approetaimation of poid function.
def fourier_function(h,tolerance = 1.0):
	order = h.size
	def f(x):
		s = 0.0
		for i in range(order):
			if np.abs(h[i])>tolerance:
				s = np.nan
			s += h[i]*np.cos((i+1)*x)
		return s
	return f

# Translate a list of arrays such that their initial values match. Cause of mismatch of 
# Fourier series approximations is the constant of integration of the surface with respect
# to the original sine wave.
# 
# ARGS:
#	List arr: List of arrays of y-coordinates.
# RETURNS:
#	List arr: List of arrays of y-coordinates, translated so the first elements match.
def equalize_starts(arr):
	cor = arr[0][0]
	for i in range(len(arr)):
		arr[i][:]-=(arr[i][0]-cor)
	return arr

if __name__ == "__main__":
	pass

	
