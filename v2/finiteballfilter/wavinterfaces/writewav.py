# This file takes a processed waveform and writes it to a WAV file to be played 
# back or saved elsewhere.

import numpy as np
import scipy.io.wavfile as scwav

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj


# Scale file to write to 128b int precision.
# 
# ARGS:
#	Array ys: Unnormalized array to be written to WAV.
# RETURNS:
#	Array newys: Ys, normalized and cast to 16-bit integer.
def normalize_and_scale(ys):
	scale_factor = 32768 # =2^16
	newys = ys/const.MAX_HEIGHT_MODULATION
	newys *= scale_factor
	newys = np.array(newys,dtype='int16')
	return newys

# Write WAV file at 128b int precision.
# 
# ARGS:
#	String filename: Name of output file.
#	Array ys: Unnormalized array to be written to WAV.
# RETURNS:
#	None
def writefile(filename,data):
	ys = normalize_and_scale(data)
	scwav.write(filename,const.SAMPLERATE,ys)

if __name__ == "__main__":
	pass