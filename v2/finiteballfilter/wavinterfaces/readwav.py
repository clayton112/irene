# This file takes a .WAV audio file and interprets it pointwise for time-domain analysis.

import warnings
import numpy as np
import scipy.io.wavfile as scwav

# *** NOTE ***
# Many WAV files maintain saved metadata in the last few bytes.
# Numpy raises a warning if this occurs, it cannot interpret it.
# We ignore warnings to avoid this being printed out on every run.
warnings.filterwarnings("ignore")

#Initially constants are uninitialized, can only be initialized in main.py.
const = None

# Links to a constants object for use throughout the file. See main.set_all_constants().
# 
# ARGS:
#	Constants constObj: constants.Constants() object, created in main.py.
# RETURNS:
#	None
def set_constants(constObj):
	global const
	const = constObj



# Rescale input file values for physical time-domain processing.
# 
# ARGS:
#	Array ys: Unnormalized int or normalized float array read from WAV.
# RETURNS:
#	Array newys: Ys, normalized to floats and modulated by value in Constants.
def normalize_and_scale(ys):
	scale_factor = 32768
	if ys.dtype == np.dtype('int16'):
		ys = np.array(ys,dtype = float)
		ys /= scale_factor
	ys *= const.MAX_HEIGHT_MODULATION
	return ys

# Read from WAV file and save as normalized array. Also, set values in Constants based on WAV data.
# This is the only function outisde main.py that can modify the Constants object. 
# 
# ARGS:
#	String filename: Name of file to be read from.
# RETURNS:
#	Array ys: Normalized data from WAV file.
def readfile(filename):
	rate,data = scwav.read(filename)
	const.set_wav_constants(rate)
	return normalize_and_scale(data)

if __name__ == "__main__":
	pass