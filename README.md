# README #

### What is this repository for? ###

This repository is for use simulating a finitely-sized stylus needle passing over the digital "surface" of a sound file. In effect, acts as a low-pass filter while adding some notable harmonic distortion. Recommended for use in digitally replicating "tracing distortion" effect.

To use: in the terminal, run:

$~ python main.py applyfilter yourwavfile.wav needleradius(cm)

The needle radius is optional. If not supplied r = 9.5e-3 cm
Afterwards, a file with the identical name as the original with "-filtered" appended to the end should appear in the same folder as the original file.

The file "ghosts_of_temp.py" is not a file that will run, but in order to reproduce pictures from the published article, copy and paste the relevant code snippets into the if todo == "temp": clause in "main.py". The code has been edited since the snippets' last use so may require some debug, and may require resizing or moving the Matplotlib window as necessary.