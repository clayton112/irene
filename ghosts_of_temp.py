
# Generate: tracing_distortion_exists.png

	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	const.set_radius(0.001)
	const.set_wav_constants(1000000)
	xs,ys1 = sampleinputs.sinwave(100,.03)
	ys2 = -.2*sampleinputs.sinwave(150,.03)[1]
	ys3 = .6*sampleinputs.sinwave(20,.03)[1]
	ys = 2*(ys1+ys2+ys3)+2
	gammax,gammay = processing.gamma(ys)
	cx,cy = processing.condense(xs,gammax,gammay)
	x1 = .00764631
	y1 = np.interp(np.array([x1]),cx,cy)[0]
	x2 = .0122545
	y2 = np.interp(np.array([x2]),cx,cy)[0]
	x3 = .00410829
	y3 = np.interp(np.array([x3]),cx,cy)[0]
	circ1x,circ1y = sampleinputs.circle(x1,y1,const.R)
	circ2x,circ2y = sampleinputs.circle(x2,y2,const.R)
	circ3x,circ3y = sampleinputs.circle(x3,y3,const.R)
	ax.plot(circ1x,circ1y,'g',label = "_nolabel_")
	ax.plot(circ2x,circ2y,'g',label = "_nolabel_")
	ax.plot(circ3x,circ3y,'g',label = "_nolabel_")
	line1 = ax.plot(np.array([x1,x2,x3]),np.array([y1,y2,y3]),"go",label = "Needle Positions")
	ysstar = ys + const.R
	line2 = ax.plot(xs,cy,'-',label = "Poid")
	line3 = ax.plot(xs,ysstar,'r-.',label = "Undistorted Playback")
	line4 = ax.fill_between(xs,ys,label = "Surface")
	ax.axis([0.000576445,.022019,1.99543,2.01321])
	ax.legend()
	plt.show()

# Generate: tans-and-ns.png
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)



	const.set_wav_constants(1000000)
	xs,ys = sampleinputs.sinwave(200,.1)
	ysdiff = processing.ddx(ys)
	x1 = .0083
	y1 = np.interp(np.array([x1]),xs,ys)[0]
	t1x = 1
	t1y = np.interp(np.array([x1]),xs,ysdiff)[0]
	n1x = -np.interp(np.array([x1]),xs,ysdiff)[0]
	n1y = 1
	scale = .0003
	line0 = ax.plot(xs,ys,'-',label = "Surface")
	t1 = ax.arrow( x1,y1,t1x*scale,t1y*scale,fc = "r",ec="r", width = .00001,head_width=0.00005, head_length=0.0001)
	n1 = ax.arrow( x1,y1,n1x*scale,n1y*scale,fc = "r",ec="r", width = .00001,head_width=0.00005, head_length=0.0001)
	ax.plot(x1,y1,"ro")
	ax.text(x1-.00038,y1,r'$\eta_1$')
	ax.text(x1+t1x*scale-.0008,y1+t1y*scale,r'$\mathbf{t}(\eta_1)$')
	ax.text(x1+n1x*scale-.00075,y1+n1y*scale,r'$\mathbf{n}(\eta_1)$')
	x2 = .0102
	y2 = np.interp(np.array([x2]),xs,ys)[0]
	t2x = 1
	t2y = np.interp(np.array([x2]),xs,ysdiff)[0]
	n2x = -np.interp(np.array([x2]),xs,ysdiff)[0]
	n2y = 1
	ax.plot(x2,y2,"ro")
	t1 = ax.arrow( x2,y2,t2x*scale,t2y*scale,fc = "r",ec="r", width = .00001,head_width=0.00005, head_length=0.0001)
	n1 = ax.arrow( x2,y2,n2x*scale,n2y*scale,fc = "r",ec="r", width = .00001,head_width=0.00005, head_length=0.0001)
	ax.text(x2+.0002,y2,r'$\eta_2$')
	ax.text(x2+t2x*scale-.0008,y2+t2y*scale,r'$\mathbf{t}(\eta_2)$')
	ax.text(x2+n2x*scale-.0008,y2+n2y*scale,r'$\mathbf{n}(\eta_2)$')



	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	ax.set_xlim([.003,.014])
	ax.set_ylim([-.0051,.0035])
	plt.show()


#generate: gammadistort, gammaerror
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.00017)
	const.set_wav_constants(1000000)
	xs,ys1 = sampleinputs.sinwave(200,.02)
	xs,ys2 = sampleinputs.sinwave(300,.02)
	ys = ys1+ys2
	gammax,gammay = processing.gamma(ys)
	yexp = ys + const.R
	line2 = ax.plot(xs,ys,label = 'Surface')
	line1 = ax.plot(gammax,gammay,label = 'Normal Curve')
	line2 = ax.plot(xs,yexp,"k-.",label = 'Undistorted')
	ax.set_xlim([.00365,.006338])
	ax.set_ylim([-.0015,.0015])
	ax.set_title("Tracing Distortion")
	ax.legend()

	plt.show()

#generate: gammacondensed
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.0009)
	const.set_wav_constants(1000000)
	xs,ys = sampleinputs.sinwave(200,.02)
	ys+=1
	gammax,gammay = processing.gamma(ys)
	yresult = processing.condense(xs,gammax,gammay)[1]
	line2 = ax.fill_between(xs,ys,label = 'Surface')
	line1 = ax.plot(gammax,gammay,"b-.",label = 'Normal Curve')
	line2 = ax.plot(xs,yresult,"r",label = 'Poid')
	ax.set_title("Tracing Error")
	ax.legend()

	plt.show()

#generate: step1
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.001)
	const.set_wav_constants(300000)
	low = 0
	high = 156
	xs,ys1 = sampleinputs.sinwave(200,.025)
	ys = 2*ys1+1
	gammax,gammay = processing.gamma(ys)
	ximports = np.copy(gammax[low:high])
	yimports = np.copy(gammay[low:high])
	yexp = ys + const.R
	line1 = ax.plot(gammax,gammay,"g",label = 'Normal Curve')
	line2 = ax.plot(ximports,yimports,"r",linewidth = 3,label = 'Region I')
	line3 = ax.plot(gammax,gammay,"bo",label = 'Discrete Data')
	ax.set_title("Step 1")
	ax.text(gammax[156]+.0001,gammay[156],"StopPoint")
	ax.legend()
	ax.set_xlim([.0164,.0209])
	ax.set_ylim([.994,1.01])
	plt.show()
	#step2
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.001)
	const.set_wav_constants(300000)
	low = 156
	high = 160
	xs,ys1 = sampleinputs.sinwave(200,.025)
	ys = 2*ys1+1
	gammax,gammay = processing.gamma(ys)
	ximports = np.copy(gammax[low:high])
	yimports = np.copy(gammay[low:high])
	yexp = ys + const.R
	line1 = ax.plot(gammax,gammay,"g",label = 'Normal Curve')
	line2 = ax.plot(ximports,yimports,"r",linewidth = 3,label = 'Region II')
	line3 = ax.plot(gammax,gammay,"bo",label = 'Discrete Data')
	ax.set_title("Step 2")
	ax.text(gammax[156]+.0001,gammay[156],"StopPoint")
	ax.text(gammax[160]-.00065,gammay[160]-.0002,"StartPoint")
	ax.legend()
	ax.set_xlim([.0164,.0209])
	ax.set_ylim([.994,1.01])
	plt.show()
	#step3
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.001)
	const.set_wav_constants(300000)
	low = 156
	high = 160
	xs,ys1 = sampleinputs.sinwave(200,.025)
	ys = 2*ys1+1
	gammax,gammay = processing.gamma(ys)
	ux = np.copy(gammax[143:156])
	uy = np.copy(gammay[143:156])
	vx = np.copy(gammax[159:172])
	vy = np.copy(gammay[159:172])
	x,y = processing.curves_intersect(ux,uy,vx,vy)
	yexp = ys + const.R
	line1 = ax.plot(gammax,gammay,"g-.",label = '_labelless_NormalCurve_')
	line2 = ax.plot(ux,uy,"b--",linewidth = 2.5, label = "u")
	line4 = ax.plot(vx,vy,"r",linewidth = 2.5, label = "v")
	line3 = ax.plot(gammax,gammay,"go",label = '_labelless_Discrete Data_')
	point1 = ax.plot(x,y,"ko",label="Intersection Point")
	ax.set_title("Step 3")
	ax.text(gammax[156]+.0001,gammay[156],"StopPoint")
	ax.text(gammax[160]-.00065,gammay[160]-.0002,"StartPoint")
	ax.axvspan(gammax[159], gammax[155], color='b', alpha=0.2, lw=0,label = "Intersecting Region")
	ax.legend(numpoints=1)
	ax.set_xlim([.0164,.0209])
	ax.set_ylim([.994,1.01])
	plt.show()
	#step4
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.001)
	const.set_wav_constants(300000)
	low = 156
	high = 160
	xs,ys1 = sampleinputs.sinwave(200,.025)
	ys = 2*ys1+1
	gammax,gammay = processing.gamma(ys)
	cx,cy = processing.condense(xs,gammax,gammay)
	ux = np.copy(gammax[143:156])
	uy = np.copy(gammay[143:156])
	vx = np.copy(gammax[159:172])
	vy = np.copy(gammay[159:172])
	x,y = processing.curves_intersect(ux,uy,vx,vy)
	uvx,uvy = processing.take_sections(ux,uy,vx,vy)
	yexp = ys + const.R
	line1 = ax.plot(gammax,gammay,"g-.",label = '_labelless_NormalCurve_')
	line2 = ax.plot(cx,cy,"g--",linewidth = 2.5, label = "_Region III_")
	line2 = ax.plot(uvx,uvy,"b",linewidth = 2.5, label = "Region III")
	line3 = ax.plot(gammax,gammay,"k.",label = '_labelless_Discrete Data_')
	line3 = ax.plot(cx,cy,"ro",label = 'Output Points')
	ax.set_title("Step 4")
	ax.text(gammax[156]+.0001,gammay[156],"StopPoint")
	ax.text(gammax[160]-.00065,gammay[160]-.0002,"StartPoint")
	ax.legend()
	ax.set_xlim([.0164,.0209])
	ax.set_ylim([.994,1.01])
	plt.show()
	#error1
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.001)
	low = 677
	high = 681
	ys1 = readwav.readfile("WAVstuff/WAVfiles/0279-full_-90_1800_21_18000_1400_4.pri0-0-BN180-E0.2-BK100-FL200-OUT1-1-D1-44100-18000.wav")[:1000]
	xs = processing.getx(ys1)
	ys = 2*ys1+1
	gammax,gammay = processing.gamma(ys)
	cx,cy = processing.condense(xs,gammax,gammay)
	trackx= np.copy(gammax[low:high])
	tracky= np.copy(gammay[low:high])
	line1 = ax.plot(gammax,gammay,"b-.",label = 'Normal Curve')
	line2 = ax.plot(cx,cy,"k--",label = 'Output')
	line3 = ax.plot(cx,cy,"ko",label = '_labelless_NormalCurve_')
	line4 = ax.plot(gammax,gammay,"b.",label = '_labelless_NormalCurve_')
	line5 = ax.plot(trackx,tracky,"g",label = 'Error Region')
	line6 = ax.fill_between(xs,ys,label = '_labelless_NormalCurve_')
	line6 = ax.plot(xs,ys,"k.",label = '_labelless_NormalCurve_')
	line7 = ax.plot(np.array([gammax[677],gammax[680]]),np.array([gammay[677],gammay[680]]),"r",linewidth = 2,label = "Interpolant Line")
	ax.set_title("Possible Error 1")
	ax.legend()
	ax.text(gammax[678]+.0001,gammay[678],"StopPoint")
	ax.text(gammax[679]-.00065,gammay[679]+.0002,"StartPoint")
	ax.set_ybound([.998,1.002])
	ax.set_xbound([0.548,.554])
	plt.show()
	#error2
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)


	const.set_radius(.002)
	low = 677
	high = 681
	ys1 = readwav.readfile("WAVstuff/WAVfiles/0279-full_-90_1800_21_18000_1400_4.pri0-0-BN180-E0.2-BK100-FL200-OUT1-1-D1-44100-18000.wav")[:1000]
	xs = processing.getx(ys1)
	ys = 2*ys1+1
	gammax,gammay = processing.gamma(ys)
	cx,cy = processing.condense(xs,gammax,gammay)
	trackx= np.copy(gammax[low:high])
	tracky= np.copy(gammay[low:high])
	line1 = ax.plot(gammax,gammay,"g",label = 'Normal Curve')
	line2 = ax.plot(cx,cy,"k--",label = 'Output')
	line3 = ax.plot(cx,cy,"ko",label = '_labelless_NormalCurve_')
	line4 = ax.plot(gammax,gammay,"b.",label = '_labelless_NormalCurve_')
	line6 = ax.fill_between(xs,ys,label = 'Surface')
	line6 = ax.plot(xs,ys,"k.",label = '_labelless_NormalCurve_')
	ax.set_title("Possible Error 2")
	ax.legend()
	ax.text(gammax[678]+.0001,gammay[678],"StopPoint")
	ax.text(gammax[679]-.00065,gammay[679]+.0002,"StartPoint")
	ax.set_ybound([.9979,.997+.0071])
	ax.set_xbound([0.576,.5919])
	plt.show()
	#eta0_err
	from scipy.optimize import brentq
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)

	A,R,X = const.A,const.R,.0001
	rx = freqspacefilter.make_rx(A,R,X)
	rxprime = freqspacefilter.make_rxprime(A,R,X)
	high = brentq(lambda x: 1+ rxprime(x),-.5*X,0)
	x0 = brentq(lambda x: x+rx(x),-.5*X,high)
	important_pointsx = np.array([-.5*X,x0,high,0])
	important_pointsdx = np.array([-.5*X,high,0])
	important_pointsy = important_pointsx+rx(important_pointsx)
	important_pointsdy = (1+rxprime(important_pointsdx))*.00002
	x = np.linspace(-.5*X,0,10000)
	y = x+rx(x)
	yprime = (1+rxprime(x))*.00002
	ax.plot(x,y,"g",label = r'$\Gamma_x(\eta)$')
	ax.plot(x,yprime,"r",label = r"$\Gamma_x'(\eta)$")
	ax.plot(x,0*yprime,"k-.")
	ax.plot(important_pointsx,important_pointsy,"go")
	ax.plot(important_pointsdx,important_pointsdy,"ro")
	ax.set_title("Tracing Error")
	ax.legend()
	ax.text(important_pointsx[1]+.0000003,important_pointsy[1]-.000003,r'$\eta_0$')
	ax.text(important_pointsx[2]-.000004,important_pointsy[2]+.000003,'Critical Point')
	ax.set_xbound([-.5*X,0.0])
	ax.set_ybound([-.00006,.00006])
	plt.show()
#eta0_distortion
	from scipy.optimize import brentq
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)

	A,R,X = const.A,const.R,.0002
	rx = freqspacefilter.make_rx(A,R,X)
	rxprime = freqspacefilter.make_rxprime(A,R,X)
	x = np.linspace(-.5*X,0,10000)
	y = x+rx(x)
	yprime = (1+rxprime(x))*.00003
	special_xs = np.array([x[0],x[-1]])
	special_ys = special_xs+rx(special_xs)
	ax.plot(x,y,"g",label = r'$\Gamma_x(\eta)$')
	ax.plot(x,yprime,"r",label = r"$\Gamma_x'(\eta)$")
	ax.plot(x,0*yprime,"k-.")
	ax.plot(special_xs,special_ys,"b--",label = "Undistorted")
	ax.set_title("Tracing Distortion")
	ax.legend()
	plt.show()

#eta0_meaning_dist
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)

	const.set_wav_constants(10000000)
	A,R = const.A,const.R
	f_0 = 100000
	X = const.v/(f_0)
	etas,ys = sampleinputs.coswave(1/X,.5*X,start = -.5*X)
	eta0 = freqspacefilter.get_eta0(A,R,X)
	ys = -1*ys
	exys = ys + const.R
	f_eta0 = np.interp(np.array([eta0]),etas,ys)[0]
	rx = freqspacefilter.make_rx(A,R,X)
	ry = freqspacefilter.make_ry(A,R,X)
	gamma_eta0x = eta0 + rx(eta0)
	gamma_eta0y =  f_eta0+ ry(eta0)
	gammax = etas + rx(etas)
	gammay = ys + ry(etas)
	specialx = np.array([eta0,gamma_eta0x])
	specialy = np.array([f_eta0,gamma_eta0y])
	ax.plot(etas,exys,"b--",linewidth = 1,label = "Undistorted Sound")
	ax.plot(etas,ys,"b",linewidth = 2,label = r'$\mathbf{p}(\eta)$')
	ax.plot(gammax,gammay,"g",label = r'$\Gamma_x(\eta)$')
	ax.plot(specialx,specialy,"r",linewidth = 3,label = r'$\mathbf{\Gamma}(\eta_0)$')
	ax.plot(specialx,specialy,"ro",label = "_nolabel_")
	ax.set_title("Tracing Distortion")
	ax.set_ybound([-.004,.022])
	ax.text(specialx[0],specialy[0]-.0006,r'$\eta_0$')

	ax.legend()
	plt.show()

#eta0-ex-dist/err
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)

	R = const.R/2
	f_0 = 2500
	A,X = freqspacefilter.get_AX(f_0,const.A,constvel = False)
	const.set_wav_constants(10000*f_0)
	etas,ys = sampleinputs.coswave(f_0,X,start = -.5*X,scaled = False)
	eta0 = freqspacefilter.get_eta0(A,R,X)
	ys = -1*ys*A
	exys = ys + R
	f_eta0 = np.interp(np.array([eta0]),etas,ys)[0]
	rx = freqspacefilter.make_rx(A,R,X)
	ry = freqspacefilter.make_ry(A,R,X)
	gamma_eta0x = eta0 + rx(eta0)
	gamma_eta0y =  f_eta0+ ry(eta0)
	circlex,circley = sampleinputs.circle(gamma_eta0x,gamma_eta0y,R)

	gammax = etas + rx(etas)
	gammay = ys + ry(etas)
	"""otherlines = sampleinputs.show_norms(etas,ys,gammax,gammay,pos = 2000)
	for i in range(len(otherlines[1])):
		ax.plot(otherlines[0][i],otherlines[1][i],"r")"""
	specialx = np.array([eta0,gamma_eta0x])
	specialy = np.array([f_eta0,gamma_eta0y])
	ax.plot(etas,ys,"b",linewidth = 2,label = r'$\mathbf{p}(\eta)$')
	ax.plot(gammax,gammay,"g",linewidth=3,label = r'$\Gamma_x(\eta)$')
	ax.plot(etas,exys,"b-.",linewidth = 1,label = "Undistorted Sound")
	ax.plot(specialx,specialy,"r",linewidth = 3,label = r'$\mathbf{\Gamma}(\eta_0)$')
	ax.plot(specialx,specialy,"ro",label = "_nolabel_")
	ax.set_title("Tracing Error")
	ax.set_ylim([-.003,.015])
	ax.text(specialx[0],specialy[0]-.0006,r'$\eta_0$')

	ax.legend()
	plt.show()
